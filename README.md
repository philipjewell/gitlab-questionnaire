# topN
Description: To fulfill Q5 on the gitlab questionnaire

>Write a program, topN, that given an arbitrarily large file and a number, N, containing individual numbers on each line (e.g. 200Gb file), will output the largest N numbers, highest first.

```
usage: topN.py [-h] -N LIMIT_NUM --file FILE_NAME

optional arguments:
  -h, --help        show this help message and exit
  -N LIMIT_NUM      N of the largest numbers to output
  --file FILE_NAME  File (path) containing individual numbers on each line
```
