#!/usr/bin/env python
import os.path
import argparse

def parse_file(file_name):
    """
    Convert file_name into a list
    :param file_name:
    :return: lines: list
    """
    with open(file_name) as f:
        lines = f.read().splitlines()
    return lines

def sort_and_limit(num_list, limit_num):
    """
    Take the list of numbers and return only the top limit_num
    :param num_list: list, or numbers that need to be sorted and limited
    :param limit_num: int, number of items to return
    :return: list, of numbers sorted and only limit_num of them
    """
    int_list = [int(x) for x in num_list]
    int_list.sort(reverse=True)
    return int_list[:limit_num]


def parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument(
        '-N',
        type=int,
        dest='limit_num',
        default=None,
        required=True,
        help="N of the largest numbers to output"
    )
    parser.add_argument(
        '--file',
        dest='file_name',
        default=None,
        required=True,
        help="File (path) containing individual numbers on each line"
    )
    args = parser.parse_args()
    return args

def main():
    args = parse_args()
    file_name = args.file_name

    # check if file exists
    if not os.path.isfile(file_name):
        raise FileNotFoundError("No such file or directory: '{}', please check for typos and correct file pathing.".format(file_name))

    num_list = parse_file(file_name)
    resulting_list_of_numbers = sort_and_limit(num_list, args.limit_num)
    print(resulting_list_of_numbers)


if __name__ == '__main__':
    main()

